extern crate num_derive;

use futures::sink::SinkExt; // FramedWrite::send

use std::error::Error;
use std::fs::File;
use std::io::BufReader;
use std::str;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::{Arc, Mutex};

use tokio::io::{AsyncRead, AsyncWrite};
use tokio::io::{ReadHalf, WriteHalf};
use tokio::net::TcpStream;
use tokio::stream::StreamExt;
use tokio::time::{self, Duration};
use tokio_util::codec::{FramedRead, FramedWrite};

use crate::codec::*;
use crate::connection_status::ConnectionStatus;
use crate::default_args::DefaultArgs;
use crate::irc_items::*;

pub(crate) trait Connectable<R, W> {
  // TODO rework the Result type
  #[allow(clippy::type_complexity)]
  fn connect(&self) -> Result<(FramedRead<R, IrcCodec>, FramedWrite<W, IrcCodec>), IrcTaskError>;
}

pub(crate) struct Connection {
  server: String,
}

impl Connection {
  pub(crate) fn new(addr: &str) -> Self {
    Connection {
      server: String::from(addr),
    }
  }
}

impl Connectable<ReadHalf<TcpStream>, WriteHalf<TcpStream>> for Connection {
  // TODO rework the Result type
  #[allow(clippy::type_complexity)]
  fn connect(
    &self,
  ) -> Result<
    (
      FramedRead<ReadHalf<TcpStream>, IrcCodec>,
      FramedWrite<WriteHalf<TcpStream>, IrcCodec>,
    ),
    IrcTaskError,
  > {
    let stream = futures::executor::block_on(TcpStream::connect(self.server.clone()))?;
    let (read, write) = tokio::io::split(stream);
    let read = FramedRead::new(read, IrcCodec::default());
    let write = FramedWrite::new(write, IrcCodec::default());
    Ok((read, write))
  }
}

pub(crate) trait RegisteredCommandsProvider {
  fn get_commands(&self) -> Vec<RegisteredCommand>;
}

pub(crate) struct FileRegisteredCommands {
  path: Option<String>,
}

impl FileRegisteredCommands {
  pub(crate) fn new(path: &Option<String>) -> Self {
    FileRegisteredCommands { path: path.clone() }
  }
}

impl RegisteredCommandsProvider for FileRegisteredCommands {
  fn get_commands(&self) -> Vec<RegisteredCommand> {
    if let Some(path) = &self.path {
      if let Ok(file) = File::open(path) {
        let reader = BufReader::new(file);
        return serde_json::from_reader(reader).unwrap_or_default();
      }
    }
    vec![]
  }
}

#[derive(Debug, Clone, PartialEq)]
pub(crate) enum IrcTaskError {
  ConnectionError,
}

impl From<std::io::Error> for IrcTaskError {
  fn from(_: std::io::Error) -> Self {
    IrcTaskError::ConnectionError
  }
}

pub(crate) struct IrcClient {
  /// Commands to be send
  commands: Arc<Mutex<Vec<IrcCommand>>>,
  /// Commands provided by the script, waits for server message to be process
  registered_commands: Arc<Mutex<Vec<RegisteredCommand>>>,
  /// Whether connected to server
  connected: Arc<AtomicBool>,
  /// Whether task is active
  is_active: Arc<AtomicBool>,
}

impl IrcClient {
  pub fn new() -> Self {
    IrcClient {
      commands: Arc::new(Mutex::new(Vec::<IrcCommand>::new())),
      registered_commands: Arc::new(Mutex::new(Vec::<RegisteredCommand>::new())),
      connected: Arc::new(AtomicBool::new(false)),
      is_active: Arc::new(AtomicBool::new(false)),
    }
  }

  pub(crate) fn register_user(&self, args: DefaultArgs) {
    const MODE: UserMode = UserMode::LocalOperator;
    if let Ok(mut commands) = self.commands.lock() {
      if let Some(password) = args.password {
        commands.push(IrcCommand::Pass(Pass { password }));
      }
      if let Some(nick) = args.nick {
        commands.push(IrcCommand::Nick(NickCommand { nick }));
        commands.push(IrcCommand::User(UserCommand {
          prefix: String::new(),
          username: args.username,
          mode: MODE,
          hostname: args.hostname,
          realname: args.realname,
        }));
      }
    };
  }

  pub(crate) fn register_commands<R>(&self, provider: R) -> Result<(), Box<dyn Error>>
  where
    R: RegisteredCommandsProvider,
  {
    let mut json_commands = provider.get_commands();
    let number_to_send = json_commands
      .iter()
      .take_while(|command| command.on_message == None)
      .count();

    // add commands without dependency to be executed
    if let Ok(mut commands) = self.commands.lock() {
      let to_send: Vec<IrcCommand> = json_commands
        .iter()
        .take(number_to_send)
        .map(|x| x.command.clone())
        .collect();
      commands.extend_from_slice(&to_send);
    }

    // store the rest commands
    if let Ok(mut registered_commands) = self.registered_commands.lock() {
      json_commands.drain(..number_to_send);
      registered_commands.extend_from_slice(&json_commands);
    }

    Ok(())
  }

  pub(crate) async fn start<
    R: 'static + AsyncRead + Send + Sync + Unpin,
    W: 'static + AsyncWrite + Send + Sync + Unpin,
  >(
    &self,
    connection: impl Connectable<R, W>,
  ) -> Result<tokio::task::JoinHandle<()>, IrcTaskError> {
    let (reader, writer) = connection.connect()?;
    let clone_commands = self.commands.clone();

    let clone_connected = self.connected.clone();

    self.is_active.store(true, Ordering::SeqCst);
    let clone_active = self.is_active.clone();

    Ok(tokio::spawn(async move {
      Self::loop_function(
        reader,
        writer,
        &clone_commands,
        &clone_connected,
        &clone_active,
      )
      .await;
    }))
  }

  async fn loop_function<W: AsyncWrite + Unpin, R: AsyncRead + Unpin>(
    r: FramedRead<R, IrcCodec>,
    mut w: FramedWrite<W, IrcCodec>,
    commands: &Mutex<Vec<IrcCommand>>,
    connected: &AtomicBool,
    is_active: &AtomicBool,
  ) {
    let mut r = r.timeout(Duration::from_secs(5));
    while is_active.load(Ordering::SeqCst) {
      // https://users.rust-lang.org/t/mutexguard-cannot-be-sent-inside-future-generator/21584/4
      let commands = {
        let mut commands = match commands.lock() {
          Ok(commands) => commands,
          Err(err) => {
            println!("error {}", err);
            err.into_inner()
          }
        };
        let clone = (*commands).clone();
        (*commands).clear();
        clone
      };

      for c in commands.into_iter() {
        let _ = w.send(c).await;
      }

      let mut connection = ConnectionStatus::default();

      if let Ok(Some(Ok(responses))) = r.try_next().await {
        for item in responses.iter() {
          match &*item {
            IrcMessage::Notice(msg) => {
              println!("NOTICE {} {:?} {}", msg.from, msg.targets, msg.message);
            }
            IrcMessage::Numeric(msg) => {
              println!("{} {}", msg.code, msg.trailer);
              if connection.check(&msg.code) {
                connected.store(true, Ordering::SeqCst);
              }
            }
            IrcMessage::Ping(msg) => {
              println!("PING {:?}", msg.params);
            }
            IrcMessage::Pong(msg) => {
              println!("PONG {:?}", msg.params);
            }
            _ => {
              // nop
            }
          }
        }
      }
    }
  }

  async fn ping_process(commands: &Mutex<Vec<IrcCommand>>, connected: &AtomicBool, hostname: &str) {
    let mut interval = time::interval(time::Duration::from_secs(5));
    loop {
      interval.tick().await;
      if connected.load(Ordering::SeqCst) {
        if let Ok(mut commands) = commands.lock() {
          commands.push(IrcCommand::Ping(Ping {
            prefix: String::new(),
            params: vec![String::from(hostname)],
            trailer: String::new(),
          }));
        }
      }
    }
  }

  pub(crate) async fn ping(&self, server: String) {
    let commands = self.commands.clone();
    let connected = self.connected.clone();
    let _ = tokio::spawn(async move {
      Self::ping_process(&commands, &connected, &server).await;
    })
    .await;
  }
}

#[cfg(test)]
#[allow(dead_code)] // consts reported as not used
mod tests {
  use super::*;
  use core::pin::Pin;
  use core::task::{Context, Poll};
  use std::collections::VecDeque;
  use std::io;
  use std::io::ErrorKind;
  use tokio::io::{AsyncRead, AsyncWrite, ReadBuf};

  /// A fake stream for testing network applications backed by buffers.
  #[derive(Clone, Debug)]
  pub struct MockStream {
    received: Vec<u8>,
    /// String representation of expected outgoing commands
    expected_outgoing: VecDeque<String>,
    /// Status whether all outgoing commands are consumed
    is_outgoing_consumed: Arc<AtomicBool>,
  }

  impl MockStream {
    /// Creates a new mock stream with nothing to read.
    pub fn empty() -> MockStream {
      MockStream::new(&[], &VecDeque::new(), &Arc::new(AtomicBool::new(false)))
    }

    /// Creates a new mock stream with the specified bytes to read.
    // TODO uniform interface of read and outgoing
    pub fn new(
      initial_read: &[u8],
      expected_outgoing: &VecDeque<String>,
      is_outgoing_consumed: &Arc<AtomicBool>,
    ) -> MockStream {
      MockStream {
        received: initial_read.to_owned(),
        expected_outgoing: expected_outgoing.to_owned(),
        is_outgoing_consumed: is_outgoing_consumed.clone(),
      }
    }
  }

  impl AsyncRead for MockStream {
    fn poll_read(
      mut self: Pin<&mut Self>,
      _: &mut Context<'_>,
      buf: &mut ReadBuf<'_>,
    ) -> Poll<io::Result<()>> {
      if !self.received.is_empty() {
        let slice = &self.received[..];
        buf.put_slice(&slice[..]);
        self.received.clear();
        Poll::Ready(io::Result::Ok(()))
      } else {
        Poll::Ready(io::Result::Err(std::io::Error::new(
          ErrorKind::Other,
          "No data!",
        )))
      }
    }
  }

  impl AsyncWrite for MockStream {
    fn poll_shutdown(
      self: std::pin::Pin<&mut Self>,
      _: &mut std::task::Context<'_>,
    ) -> std::task::Poll<std::result::Result<(), std::io::Error>> {
      Poll::Ready(io::Result::Ok(()))
    }

    fn poll_write(
      mut self: std::pin::Pin<&mut Self>,
      _: &mut std::task::Context<'_>,
      command: &[u8],
    ) -> std::task::Poll<std::result::Result<usize, std::io::Error>> {
      // Verifies whether first expected command is as in the buffer
      assert_eq!(
        str::from_utf8(command).ok().map(|s| s.to_string()).as_ref(),
        self.expected_outgoing.front()
      );
      self.expected_outgoing.pop_front();
      self
        .is_outgoing_consumed
        .store(self.expected_outgoing.is_empty(), Ordering::SeqCst);
      Poll::Ready(io::Result::Ok(command.len()))
    }

    fn poll_flush(
      self: std::pin::Pin<&mut Self>,
      _: &mut std::task::Context<'_>,
    ) -> std::task::Poll<std::result::Result<(), std::io::Error>> {
      Poll::Ready(Ok(()))
    }
  }

  pub(crate) struct MockConnection {
    server: String,
    expected_outgoing: VecDeque<String>,
    is_outgoing_consumed: Arc<AtomicBool>,
  }

  impl MockConnection {
    pub(crate) fn new(
      addr: &str,
      expected_outgoing: VecDeque<String>,
      is_outgoing_consumed: &Arc<AtomicBool>,
    ) -> Self {
      MockConnection {
        server: String::from(addr),
        expected_outgoing,
        is_outgoing_consumed: is_outgoing_consumed.clone(),
      }
    }
  }

  impl Connectable<MockStream, MockStream> for MockConnection {
    #[allow(clippy::type_complexity)]
    fn connect(
      &self,
    ) -> Result<
      (
        FramedRead<MockStream, IrcCodec>,
        FramedWrite<MockStream, IrcCodec>,
      ),
      IrcTaskError,
    > {
      let read = FramedRead::new(MockStream::empty(), IrcCodec::default());
      let write = FramedWrite::new(
        MockStream::new(&[], &self.expected_outgoing, &self.is_outgoing_consumed),
        IrcCodec::default(),
      );
      Ok((read, write))
    }
  }

  pub(crate) struct TestRegisteredCommands {
    commands: Vec<RegisteredCommand>,
  }

  impl TestRegisteredCommands {
    pub(crate) fn new(commands: Vec<RegisteredCommand>) -> Self {
      TestRegisteredCommands { commands }
    }
  }

  impl RegisteredCommandsProvider for TestRegisteredCommands {
    fn get_commands(&self) -> Vec<RegisteredCommand> {
      self.commands.clone()
    }
  }

  #[tokio::test(flavor = "multi_thread")]
  async fn loop_function_notice_will_change_state_to_connected() {
    let command = format!(
      "{}{}{}{}",
      ":irc.server 001 :Welcome to IRC\x0d\x0a",
      ":irc.server 002 :Your host is IRC\x0d\x0a",
      ":irc.server 003 :Server was created on Debian\x0d\x0a",
      ":irc.server 004 Eva irc.local\x0d\x0a"
    );
    let r = FramedRead::new(
      MockStream::new(
        command.as_bytes(),
        &VecDeque::new(),
        &Arc::new(AtomicBool::new(false)),
      ),
      IrcCodec::default(),
    );
    let w = FramedWrite::new(MockStream::empty(), IrcCodec::default());

    let commands = Arc::new(Mutex::new(Vec::<IrcCommand>::new()));

    let connected = Arc::new(AtomicBool::new(false));
    let connected_clone = Arc::clone(&connected);

    let active = Arc::new(AtomicBool::new(true));
    let active_clone = active.clone();

    let task = tokio::spawn(async move {
      IrcClient::loop_function(r, w, &commands, &connected, &active).await;
    });
    while connected_clone.load(Ordering::SeqCst) == false {}

    // finish task
    active_clone.store(false, Ordering::SeqCst);
    let _ = task.await;
  }

  struct IrcClientTest {
    irc_client: IrcClient,
  }

  impl IrcClientTest {
    fn new() -> Self {
      IrcClientTest {
        irc_client: IrcClient::new(),
      }
    }

    async fn start(&self, initial_outgoing: VecDeque<String>) {
      let server = "localhost::6667";
      let is_consumed = Arc::new(AtomicBool::new(false));
      let connection = MockConnection::new(&server, initial_outgoing, &is_consumed);
      let irc = self.irc_client.start(connection);

      let is_consumed_clone = is_consumed.clone();
      let is_active_clone = self.irc_client.is_active.clone();
      let test_handler = tokio::spawn(async move {
        while !is_consumed_clone.load(Ordering::SeqCst) {}
        is_active_clone.store(false, Ordering::SeqCst);
      });
      // wait till commands will consumed by irc client, and trigger irc client to finish its task
      let (handler_result, irc_result) = tokio::join!(test_handler, irc);

      match handler_result {
        Ok(()) => assert!(true),
        _ => assert!(false),
      }

      match irc_result {
        Ok(_) => assert!(true),
        _ => assert!(false),
      }
    }

    fn register_commands(&self, commands: Vec<RegisteredCommand>) {
      self
        .irc_client
        .register_commands(TestRegisteredCommands::new(commands))
        .unwrap();
    }

    fn get_number_commands(&self) -> Option<usize> {
      if let Ok(commands) = self.irc_client.commands.lock() {
        return Some(commands.len());
      }
      None
    }

    fn get_number_registered_commands(&self) -> Option<usize> {
      if let Ok(registered) = self.irc_client.registered_commands.lock() {
        return Some(registered.len());
      }
      None
    }
  }

  fn build_user_command() -> UserCommand {
    UserCommand {
      prefix: "prefix".to_string(),
      username: "user1".to_string(),
      mode: UserMode::LocalOperator,
      hostname: "localhost".to_string(),
      realname: "realname".to_string(),
    }
  }

  fn build_nick_command() -> NickCommand {
    NickCommand {
      nick: "user1".to_string(),
    }
  }

  fn build_join_command() -> Join {
    Join {
      channels: vec!["channel1".into(), "channel2".into()],
    }
  }

  fn build_numeric_message(msg_code: NumericCode) -> NumericMessage {
    NumericMessage {
      prefix: String::new(),
      code: msg_code,
      params: vec![],
      trailer: String::new(),
    }
  }

  #[tokio::test(flavor = "multi_thread")]
  async fn irc_client_with_registered_commands_all_send() {
    let mut expected_outgoing = VecDeque::new();
    expected_outgoing.push_back(String::from("NICK user1\r\n"));
    expected_outgoing.push_back(String::from(":prefix USER user1 0 localhost :realname\r\n"));

    let client = IrcClientTest::new();
    let registered_commands = vec![
      RegisteredCommand {
        command: IrcCommand::Nick(build_nick_command()),
        on_message: None,
      },
      RegisteredCommand {
        command: IrcCommand::User(build_user_command()),
        on_message: None,
      },
    ];
    client.register_commands(registered_commands);
    client.start(expected_outgoing).await;

    assert_eq!(client.get_number_commands(), Some(0));
    assert_eq!(client.get_number_registered_commands(), Some(0));
  }

  #[tokio::test(flavor = "multi_thread")]
  async fn irc_client_with_registered_commands_one_not_send() {
    let mut expected_outgoing = VecDeque::new();
    expected_outgoing.push_back(String::from("NICK user1\r\n"));
    expected_outgoing.push_back(String::from(":prefix USER user1 0 localhost :realname\r\n"));

    let client = IrcClientTest::new();
    let registered_commands = vec![
      RegisteredCommand {
        command: IrcCommand::Nick(build_nick_command()),
        on_message: None,
      },
      RegisteredCommand {
        command: IrcCommand::User(build_user_command()),
        on_message: None,
      },
      RegisteredCommand {
        command: IrcCommand::Join(build_join_command()),
        on_message: Some(IrcMessage::Numeric(build_numeric_message(
          NumericCode::RplIsSupport,
        ))),
      },
    ];
    client.register_commands(registered_commands);
    client.start(expected_outgoing).await;

    assert_eq!(client.get_number_commands(), Some(0));
    assert_eq!(client.get_number_registered_commands(), Some(1));
  }
}
