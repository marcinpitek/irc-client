use crate::irc_items::*;

#[derive(Default)]
pub(crate) struct ConnectionStatus {
  rpl_welcome: bool,
  rpl_yourhost: bool,
  rpl_created: bool,
  rpl_myinfo: bool,
}

impl ConnectionStatus {
  pub(crate) fn check(&mut self, code: &NumericCode) -> bool {
    self.rpl_welcome = self.rpl_welcome || *code == NumericCode::RplWelcome;
    self.rpl_yourhost = self.rpl_yourhost || *code == NumericCode::RplYourHost;
    self.rpl_created = self.rpl_created || *code == NumericCode::RplCreated;
    self.rpl_myinfo = self.rpl_myinfo || *code == NumericCode::RplMyinfo;
    self.rpl_welcome && self.rpl_yourhost && self.rpl_created && self.rpl_myinfo
  }
}

#[cfg(test)]
mod tests {
  use super::*;

  #[test]
  fn check_rpl_welcome() {
    let mut connection = ConnectionStatus::default();
    assert_eq!(connection.check(&NumericCode::RplWelcome), false);
  }

  #[test]
  fn check_rpl_yourhost() {
    let mut connection = ConnectionStatus::default();
    assert_eq!(connection.check(&NumericCode::RplYourHost), false);
  }

  #[test]
  fn check_rpl_created() {
    let mut connection = ConnectionStatus::default();
    assert_eq!(connection.check(&NumericCode::RplCreated), false);
  }

  #[test]
  fn check_rpl_myinfo() {
    let mut connection = ConnectionStatus::default();
    assert_eq!(connection.check(&NumericCode::RplMyinfo), false);
  }

  #[test]
  fn check_connected() {
    let mut connection = ConnectionStatus::default();
    assert_eq!(connection.check(&NumericCode::RplMyinfo), false);
    assert_eq!(connection.check(&NumericCode::RplCreated), false);
    assert_eq!(connection.check(&NumericCode::RplYourHost), false);
    assert_eq!(connection.check(&NumericCode::RplWelcome), true);
  }
}
