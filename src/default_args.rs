use args::{Args, ArgsError};

use getopts::Occur;

const PROGRAM_DESC: &str = "Irc Client in progress";
const PROGRAM_NAME: &str = "Irc Client";

#[derive(Clone)]
pub(crate) struct DefaultArgs {
  pub(crate) server: Option<String>,
  pub(crate) nick: Option<String>,
  pub(crate) username: String,
  pub(crate) realname: String,
  pub(crate) hostname: String,
  pub(crate) password: Option<String>,
  pub(crate) commands_path: Option<String>,
}

impl DefaultArgs {
  fn new(args: Args) -> Self {
    let server = args.value_of::<String>("server").ok();
    let nick = args.value_of::<String>("nick").ok();
    let username = args
      .value_of::<String>("username")
      .unwrap_or_else(|_| String::from("username"));
    let realname = args
      .value_of::<String>("realname")
      .unwrap_or_else(|_| String::from("realname"));
    let hostname = args
      .value_of::<String>("hostname")
      .unwrap_or_else(|_| String::from("localhost"));
    let password = args.value_of::<String>("password").ok();
    let commands_path = args.value_of::<String>("commands").ok();
    DefaultArgs {
      server,
      nick,
      username,
      realname,
      hostname,
      password,
      commands_path,
    }
  }

  pub(crate) fn parse(input: &[String]) -> Result<DefaultArgs, ArgsError> {
    let mut args = Args::new(PROGRAM_NAME, PROGRAM_DESC);
    args.option(
      "",
      "server",
      "Address of default IRC server",
      "SERVER",
      Occur::Optional,
      None,
    );
    args.option("", "nick", "Default nick", "NICK", Occur::Optional, None);
    args.option(
      "",
      "username",
      "Default user name",
      "USERNAME",
      Occur::Optional,
      None,
    );
    args.option(
      "",
      "realname",
      "Real name",
      "REALNAME",
      Occur::Optional,
      None,
    );
    args.option(
      "",
      "hostname",
      "Host name",
      "HOSTNAME",
      Occur::Optional,
      None,
    );
    args.option(
      "",
      "password",
      "User Password",
      "PASSWORD",
      Occur::Optional,
      None,
    );
    args.option(
      "",
      "commands",
      "Path to json with serialized commands",
      "COMMANDS",
      Occur::Optional,
      None,
    );
    args.parse(input)?;

    Ok(DefaultArgs::new(args))
  }
}

#[cfg(test)]
mod tests {
  use super::*;

  #[test]
  fn check_args() {
    let input = vec![
      String::from("--server"),
      String::from("127.0.0.1:6667"),
      String::from("--nick"),
      String::from("admin"),
      String::from("--username"),
      String::from("eva"),
      String::from("--hostname"),
      String::from("my_host"),
      String::from("--password"),
      String::from("123456"),
      String::from("--realname"),
      String::from("real_eva"),
    ];
    let args = DefaultArgs::parse(&input).unwrap();
    assert_eq!(args.server, Some(String::from("127.0.0.1:6667")));
    assert_eq!(args.nick, Some(String::from("admin")));
    assert_eq!(args.username, String::from("eva"));
    assert_eq!(args.realname, String::from("real_eva"));
    assert_eq!(args.hostname, String::from("my_host"));
    assert_eq!(args.password, Some(String::from("123456")));
  }

  #[test]
  fn check_default_args() {
    let input = vec![
      String::from("--server"),
      String::from("127.0.0.1:6667"),
      String::from("--nick"),
      String::from("admin"),
    ];
    let args = DefaultArgs::parse(&input).unwrap();
    assert_eq!(args.server, Some(String::from("127.0.0.1:6667")));
    assert_eq!(args.nick, Some(String::from("admin")));
    assert_eq!(args.username, String::from("username"));
    assert_eq!(args.realname, String::from("realname"));
    assert_eq!(args.hostname, String::from("localhost"));
    assert_eq!(args.password, None);
  }
}
