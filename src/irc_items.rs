use serde::Deserialize;

use std::fmt;

#[derive(Debug, Clone, PartialEq, Deserialize)]
pub enum UserMode {
  NotDefined,
  Away,
  Invisible,
  WallopsActivate,
  RestrictedConnection,
  Operator,
  LocalOperator,
}

impl fmt::Display for UserMode {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    match *self {
      UserMode::NotDefined => Err(fmt::Error),
      UserMode::Away => write!(f, "a"),
      UserMode::Invisible => write!(f, "i"),
      UserMode::WallopsActivate => write!(f, "w"),
      UserMode::RestrictedConnection => write!(f, "r"),
      UserMode::Operator => write!(f, "o"),
      UserMode::LocalOperator => write!(f, "0"),
    }
  }
}

impl<'a> From<&str> for UserMode {
  fn from(input: &str) -> Self {
    match &input[..1] {
      "a" => UserMode::Away,
      "i" => UserMode::Invisible,
      "w" => UserMode::WallopsActivate,
      "r" => UserMode::RestrictedConnection,
      "o" => UserMode::Operator,
      "0" => UserMode::LocalOperator,
      _ => UserMode::NotDefined,
    }
  }
}

enum_from_primitive! {
#[derive(Copy, Debug, Clone, PartialEq, Deserialize)]
pub enum NumericCode {
  InternalNotRecognize = 0,
  RplWelcome = 1,
  RplYourHost = 2,
  RplCreated = 3,
  RplMyinfo = 4,
  RplIsSupport = 5,
  RplBouce = 10,
  RplUserModeIs = 221,
  RplLUserCommand = 251,
  RplLUserOp = 252,
  RplLUserUnknown = 253,
  RplLUserChannels = 254,
  RplLUserMe = 255,
  RplAdminMe = 256,
  RplAdminLoc1 = 257,
  RplAdminLoc2 = 258,
  RplAdminEmail = 259,
  RplTryAgain = 263,
  RplLocalUsers = 265,
  RplGlobalUsers = 266,
  RplWhoIsCertificateFingerprint = 276,
  RplNone = 300,
  RplAway = 301,
  RplUserHost = 302,
  RplIson = 303,
  RplUnaway = 305,
  RplNoaway = 306,
  RplWhoIsUser = 311,
  RplWhoIsServer = 312,
  RplWhoIsOperator = 313,
  RplWhoWasUser = 314,
  RplWhoIsIdle = 317,
  RplEndOfWhoIs = 318,
  RplWhoIsChannels = 319,
  RplListStart = 321,
  RplList = 322,
  RplListEnd = 323,
  RplChannelModeIs = 324,
  RplCreationTime = 329,
  RplNoTopic = 331,
  RplTopic = 332,
  RplTopicWhoTime = 333,
  RplInviting = 341,
  RplInvitelist = 346,
  RplEndOfInviteList = 347,
  RplExceptList = 348,
  RplEndOfExceptList = 349,
  RplVersion = 351,
  RplNamesReply = 353,
  RplEndOfNames = 366,
  RplBanList = 367,
  RplEndOfBanList = 368,
  RplEndOfWhoWas = 369,
  RplMessageOfTheDayStart = 375,
  RplMessageOfTheDay = 372,
  RplEndOfMessageOfTheDay = 376,
  RplYourOper = 381,
  RplRehashing = 382,
  ErrUnknownError = 400,
  ErrNoSuchNick = 401,
  ErrNoSuchServer = 402,
  ErrNoSuchChannel = 403,
  ErrCannotSendToChannel = 404,
  ErrTooManyChannels = 405,
  ErrUnknownCommand = 421,
  ErrNoMessageOfTheDay = 422,
  ErrErroneusNickname = 432,
  ErrNickNameInUse = 433,
  ErrUserNotOnThatChannel = 441,
  ErrNotOnChannel = 442,
  ErrUserOnChannel = 443,
  ErrNotRegistered = 451,
  ErrNeedMoreParams = 461,
  ErrAlreadyRegistered = 462,
  ErrPasswordMismatch = 464,
  ErrYoureBannedCreep = 465,
  ErrChannelIsFull = 471,
  ErrUnknownMode = 472,
  ErrInviteOnlyChannel = 473,
  ErrBannedFromChannel = 474,
  ErrBadChannelKey = 475,
  ErrNoPrivileges481 = 481,
  ErrChannelOpPrivilegesIsNeeded = 482,
  ErrCantKillServer = 483,
  ErrNoOperHost = 491,
  ErrUnknownModeFlag = 501,
  ErrUsersDontMatch = 502,
  RplStartTls = 670,
  ErrStarTls = 691,
  ErrNoPrivileges723 = 723,
  RplLoggedIn = 900,
  RplLoggedOut = 901,
  ErrNickLocked = 902,
  RplSaslAuthenticationFailed = 904,
  ErrSaslMessageTooLong = 905,
  ErrSaslAuthenticationAborted = 906,
  ErrSaslAlreadyUsing = 907,
  RplSaslMechanisms = 908,
}
}

impl fmt::Display for NumericCode {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    write!(f, "{:003}", *self as u32)
  }
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
pub struct Channel {
  pub channel: String,
  pub key: Option<String>,
}

impl Channel {
  pub fn new(channel: &str, key: &str) -> Self {
    Channel {
      channel: channel.to_string(),
      key: Some(key.to_string()),
    }
  }
}

impl<'a> From<&str> for Channel {
  fn from(channel: &str) -> Self {
    Channel {
      channel: channel.to_string(),
      key: None,
    }
  }
}

/// Sends to server
#[derive(Debug, Clone, PartialEq, Deserialize)]
pub struct Join {
  pub channels: Vec<Channel>,
}

/// Sends to server
#[derive(Debug, Clone, PartialEq, Deserialize)]
pub struct List {
  pub channels: Vec<String>,
}

// TODO parsing of specific Numeric message (001, 002, ...)
#[derive(Debug, Clone, PartialEq, Deserialize)]
pub struct NumericMessage {
  pub prefix: String,
  pub code: NumericCode,
  pub params: Vec<String>,
  pub trailer: String,
}

/// Sends to server
#[derive(Debug, Clone, PartialEq, Deserialize)]
pub struct NickCommand {
  pub nick: String,
}

/// Receives from server
#[derive(Debug, Clone, PartialEq, Deserialize)]
pub struct NickMessage {
  pub prefix: String,
  pub nick: String,
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
pub struct NoticeCommand {
  pub targets: Vec<String>,
  pub message: String,
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
pub struct NoticeMessage {
  pub from: String,
  pub targets: Vec<String>,
  pub message: String,
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
pub struct Oper {
  pub name: String,
  pub password: String,
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
pub struct Pass {
  pub password: String,
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
pub struct Ping {
  pub prefix: String,
  pub params: Vec<String>,
  pub trailer: String,
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
pub struct Pong {
  pub prefix: String,
  pub params: Vec<String>,
  pub trailer: String,
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
pub struct QuitMessage {
  pub from: String,
  pub message: String,
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
pub struct QuitCommand {
  pub message: String,
}

#[derive(Debug, Clone, PartialEq, Deserialize)]
pub struct UserCommand {
  pub prefix: String,
  pub username: String,
  pub mode: UserMode,
  pub hostname: String,
  pub realname: String,
}

/// Represents command provided from the script
#[derive(Debug, Clone, PartialEq, Deserialize)]
pub struct RegisteredCommand {
  pub command: IrcCommand,
  /// Expected message to trigger the command
  pub on_message: Option<IrcMessage>,
}

/// Represents outgoing commands
#[derive(Debug, Clone, PartialEq, Deserialize)]
pub enum IrcCommand {
  Join(Join),
  List(List),
  Nick(NickCommand),
  Notice(NoticeCommand),
  Oper(Oper),
  Pass(Pass),
  Ping(Ping),
  Pong(Pong),
  Quit(QuitCommand),
  User(UserCommand),
}

/// Represents incomming messages
#[derive(Debug, Clone, PartialEq, Deserialize)]
pub enum IrcMessage {
  Nick(NickMessage),
  Notice(NoticeMessage),
  Numeric(NumericMessage),
  Ping(Ping),
  Pong(Pong),
  Quit(QuitMessage),
}
