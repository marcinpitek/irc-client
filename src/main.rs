#![allow(clippy::single_match)]
#[macro_use]
extern crate enum_primitive;
extern crate args;
extern crate getopts;
extern crate num_derive;
extern crate serde_json;

pub mod codec;
pub mod connection_status;
pub mod default_args;
pub mod irc_client;
pub mod irc_items;

use std::env;
use std::error::Error;
use std::process::exit;

use crate::default_args::DefaultArgs;
use crate::irc_client::{Connection, FileRegisteredCommands, IrcClient};

async fn connect_with_default_args(args: DefaultArgs) -> Result<(), Box<dyn Error>> {
  let client = IrcClient::new();
  if let Some(server) = args.server.clone() {
    client.register_commands(FileRegisteredCommands::new(&args.commands_path))?;
    client.register_user(args);
    let ping = client.ping(server.clone());
    let irc = client.start(Connection::new(&server));
    // TODO handle error
    let _ = tokio::join!(ping, irc);
  }
  Ok(())
}

#[tokio::main]
pub async fn main() -> Result<(), Box<dyn Error>> {
  let args: Vec<String> = env::args().collect();
  match DefaultArgs::parse(&args) {
    Ok(args) => return connect_with_default_args(args).await,
    Err(error) => {
      println!("{}", error);
      exit(1);
    }
  };
}
