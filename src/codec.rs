#![allow(clippy::single_match)]

use bytes::{BufMut, BytesMut};
use itertools::Itertools;

use std::{fmt, str};

use tokio::io::{self};
use tokio_util::codec::{Decoder, Encoder};

use crate::irc_items::*;

#[derive(Clone, Debug, Default)]
pub struct IrcCodec;

// https://internals.rust-lang.org/t/easy-lazy-conditional-formatting/8856
struct DF<T>(T)
where
  T: Fn(&mut std::fmt::Formatter) -> std::fmt::Result;
impl<T> std::fmt::Display for DF<T>
where
  T: Fn(&mut std::fmt::Formatter) -> std::fmt::Result,
{
  fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
    (self.0)(f)
  }
}

fn encode_prefix(f: &mut std::fmt::Formatter, input: &str) -> std::fmt::Result {
  if !input.is_empty() {
    write!(f, ":{} ", input)?;
  }
  Ok(())
}

fn encode_params(f: &mut std::fmt::Formatter, input: &str) -> std::fmt::Result {
  if !input.is_empty() {
    write!(f, " {}", input)?;
  }
  Ok(())
}

fn encode_trailer(f: &mut std::fmt::Formatter, input: &str) -> std::fmt::Result {
  if !input.is_empty() {
    write!(f, " :{}", input)?;
  }
  Ok(())
}

fn encode_arg(f: &mut std::fmt::Formatter, input: &str) -> std::fmt::Result {
  if !input.is_empty() {
    write!(f, "{},", input)?;
  }
  Ok(())
}

fn encode_command(prefix: &str, command: &str, params: &str, trailer: &str) -> String {
  format!(
    "{}{}{}{}\x0d\x0a",
    DF(|f| { encode_prefix(f, &prefix) }),
    command,
    DF(|f| { encode_params(f, &params) }),
    DF(|f| { encode_trailer(f, &trailer) })
  )
}

fn encode_join(list: &Join) -> String {
  // first channels with key
  let mut joined = list.channels.iter().filter(|c| c.key != None).fold(
    (String::new(), String::new()),
    |join, c| {
      let key = c.key.clone().unwrap_or_else(|| String::from("default"));
      (
        format!("{}{}", DF(|f| { encode_arg(f, &join.0) }), c.channel),
        format!("{}{}", DF(|f| { encode_arg(f, &join.1) }), key),
      )
    },
  );
  // next channels without key
  let channels = list
    .channels
    .iter()
    .filter(|c| c.key == None)
    .fold(String::new(), |join, c| {
      format!("{}{}", DF(|f| { encode_arg(f, &join) }), &c.channel)
    });
  if !channels.is_empty() {
    joined.0 = format!("{}{}", DF(|f| { encode_arg(f, &joined.0) }), channels);
  }
  format!(
    "JOIN{}{}\x0d\x0a",
    DF(|f| { encode_params(f, &joined.0) }),
    DF(|f| { encode_params(f, &joined.1) }),
  )
}

fn encode_list(list: &List) -> String {
  let params = list.channels.iter().join(",");
  format!("LIST{}\x0d\x0a", DF(|f| { encode_params(f, &params) }),)
}

fn encode_nick(nick: &NickCommand) -> String {
  format!("NICK{}\x0d\x0a", DF(|f| { encode_params(f, &nick.nick) }),)
}

fn encode_user(user: &UserCommand) -> String {
  let params = format!("{} {} {}", user.username, user.mode, user.hostname);
  encode_command(&user.prefix, &String::from("USER"), &params, &user.realname)
}

fn encode_notice(notice: &NoticeCommand) -> String {
  let params = notice.targets.iter().join(",");
  encode_command(
    &String::new(),
    &String::from("NOTICE"),
    &params,
    &notice.message,
  )
}

fn encode_oper(oper: &Oper) -> String {
  format!(
    "OPER{}{}\x0d\x0a",
    DF(|f| { encode_params(f, &oper.name) }),
    DF(|f| { encode_params(f, &oper.password) }),
  )
}

fn encode_pass(pass: &Pass) -> String {
  format!(
    "PASS{}\x0d\x0a",
    DF(|f| { encode_params(f, &pass.password) }),
  )
}

fn encode_ping(ping: &Ping) -> String {
  let params = ping.params.iter().join(" ");
  encode_command(&ping.prefix, &String::from("PING"), &params, &ping.trailer)
}

fn encode_pong(pong: &Pong) -> String {
  let params = pong.params.iter().join(" ");
  encode_command(&pong.prefix, &String::from("PONG"), &params, &pong.trailer)
}

fn encode_quit(quit: &QuitCommand) -> String {
  format!(
    "QUIT{}\x0d\x0a",
    DF(|f| { encode_trailer(f, &quit.message) })
  )
}

impl Encoder<IrcCommand> for IrcCodec {
  type Error = IrcCodecError;

  fn encode(&mut self, event: IrcCommand, buf: &mut BytesMut) -> Result<(), Self::Error> {
    match event {
      IrcCommand::Join(join) => {
        let msg = encode_join(&join);
        buf.reserve(msg.len());
        buf.put(msg.as_bytes());
      }
      IrcCommand::List(list) => {
        let msg = encode_list(&list);
        buf.reserve(msg.len());
        buf.put(msg.as_bytes());
      }
      IrcCommand::Nick(nick) => {
        let msg = encode_nick(&nick);
        buf.reserve(msg.len());
        buf.put(msg.as_bytes());
      }
      IrcCommand::Notice(notice) => {
        let msg = encode_notice(&notice);
        buf.reserve(msg.len());
        buf.put(msg.as_bytes());
      }
      IrcCommand::Oper(oper) => {
        let msg = encode_oper(&oper);
        buf.reserve(msg.len());
        buf.put(msg.as_bytes());
      }
      IrcCommand::Pass(pass) => {
        let msg = encode_pass(&pass);
        buf.reserve(msg.len());
        buf.put(msg.as_bytes());
      }
      IrcCommand::Ping(ping) => {
        let msg = encode_ping(&ping);
        buf.reserve(msg.len());
        buf.put(msg.as_bytes());
      }
      IrcCommand::Pong(pong) => {
        let msg = encode_pong(&pong);
        buf.reserve(msg.len());
        buf.put(msg.as_bytes());
      }
      IrcCommand::User(user) => {
        let msg = encode_user(&user);
        buf.reserve(msg.len());
        buf.put(msg.as_bytes());
      }
      IrcCommand::Quit(quit) => {
        let msg = encode_quit(&quit);
        buf.reserve(msg.len());
        buf.put(msg.as_bytes());
      }
    }
    Ok(())
  }
}

#[derive(Debug, PartialEq)]
struct Command<'a> {
  prefix: &'a str,
  command: &'a str,
  params: Vec<&'a str>,
  trailer: &'a str,
}

impl<'a> Command<'a> {
  fn new(resp: &'a str) -> Option<Self> {
    // prase the command in format "[:prefix] command [params] :trailer"
    let first = resp.find(':');
    let last = resp.rfind(':');
    let trailer: &str = if first != None && last != Some(0) {
      &resp[last.map(|v| v + 1)?..]
    } else {
      ""
    };

    let parts: Vec<&str> = resp.split(' ').collect();
    #[allow(unused_assignments)]
    let mut consumed: usize = 0;

    if parts.is_empty() {
      return None;
    };

    // prefix and command
    let (prefix, command) = if parts[0].find(':') == Some(0) {
      // if only prefix
      if parts.len() < 2 {
        return None;
      }
      consumed = 2;
      (&parts[0][1..], parts[1])
    } else {
      consumed = 1;
      ("", parts[0])
    };

    let params: Vec<&str> = parts
      .iter()
      .cloned()
      .skip(consumed)
      .take_while(|part| part.find(':') == None)
      .collect();

    Some(Command {
      prefix,
      command,
      params,
      trailer,
    })
  }
}

impl<'a> From<Command<'a>> for NickMessage {
  fn from(command: Command) -> Self {
    NickMessage {
      prefix: String::from(command.prefix),
      nick: String::from(command.params[0]),
    }
  }
}

impl<'a> From<Command<'a>> for NoticeMessage {
  fn from(command: Command) -> Self {
    NoticeMessage {
      from: String::from(command.prefix),
      targets: command.params.iter().map(|x| String::from(*x)).collect(),
      message: String::from(command.trailer),
    }
  }
}

impl<'a> From<Command<'a>> for NumericMessage {
  fn from(command: Command) -> Self {
    let code = command.command;
    let code = match code.parse::<u32>() {
      Ok(code) => match num::FromPrimitive::from_u32(code) {
        Some(code) => code,
        None => NumericCode::InternalNotRecognize,
      },
      Err(_) => NumericCode::InternalNotRecognize,
    };
    NumericMessage {
      prefix: String::from(command.prefix),
      code,
      params: command.params.iter().map(|x| String::from(*x)).collect(),
      trailer: String::from(command.trailer),
    }
  }
}

impl<'a> From<Command<'a>> for Ping {
  fn from(command: Command) -> Self {
    Ping {
      prefix: String::from(command.prefix),
      params: command.params.iter().map(|x| String::from(*x)).collect(),
      trailer: String::from(command.trailer),
    }
  }
}

impl<'a> From<Command<'a>> for Pong {
  fn from(command: Command) -> Self {
    Pong {
      prefix: String::from(command.prefix),
      params: command.params.iter().map(|x| String::from(*x)).collect(),
      trailer: String::from(command.trailer),
    }
  }
}

impl<'a> From<Command<'a>> for QuitMessage {
  fn from(command: Command) -> Self {
    QuitMessage {
      from: String::from(command.prefix),
      message: String::from(command.trailer),
    }
  }
}

impl Decoder for IrcCodec {
  type Item = Vec<IrcMessage>;
  type Error = IrcCodecError;

  fn decode(&mut self, buf: &mut BytesMut) -> Result<Option<Vec<IrcMessage>>, IrcCodecError> {
    if !buf.is_empty() {
      let mut result = Vec::<IrcMessage>::new();
      let line = buf.split_to(buf.len());
      let line = &line[..line.len()];
      let line = utf8(line)?;
      let responses: Vec<&str> = line.split("\x0d\x0a").collect();
      for resp in responses.into_iter() {
        if let Some(command) = Command::new(&resp) {
          if command.command == "NOTICE" {
            result.push(IrcMessage::Notice(NoticeMessage::from(command)));
          } else if command.command == "NICK" && !command.params.is_empty() {
            result.push(IrcMessage::Nick(NickMessage::from(command)));
          } else if command.command == "PING" {
            result.push(IrcMessage::Ping(Ping::from(command)));
          } else if command.command == "PONG" {
            result.push(IrcMessage::Pong(Pong::from(command)));
          } else if command.command == "QUIT" {
            result.push(IrcMessage::Quit(QuitMessage::from(command)));
          } else if command.command.len() == 3 && command.command.chars().all(|c| c.is_digit(10)) {
            result.push(IrcMessage::Numeric(NumericMessage::from(command)));
          }
        }
      }
      Ok(Some(result))
    } else {
      Ok(None)
    }
  }
}

fn utf8(buf: &[u8]) -> Result<&str, io::Error> {
  str::from_utf8(buf)
    .map_err(|_| io::Error::new(io::ErrorKind::InvalidData, "Unable to decode input as UTF8"))
}

/// An error occured while encoding or decoding a line.
#[derive(Debug)]
pub enum IrcCodecError {
  /// An IO error occured.
  Io(io::Error),
}

impl fmt::Display for IrcCodecError {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    match self {
      IrcCodecError::Io(e) => write!(f, "{}", e),
    }
  }
}

impl From<io::Error> for IrcCodecError {
  fn from(e: io::Error) -> IrcCodecError {
    IrcCodecError::Io(e)
  }
}

impl std::error::Error for IrcCodecError {}

#[cfg(test)]
#[allow(dead_code)] // consts reported as not used
mod tests {
  use super::*;

  const PREFIX: &str = "irc_server";
  const COMMAND: &str = "NOTICE";
  const PARAM1: &str = "Auth";
  const PARAM2: &str = "AWAYLEN=200";
  const PARAM3: &str = "KICKLEN=255";
  const TRAILER: &str = "are supported by this server";

  #[test]
  fn command_parser() {
    let response = format!(
      ":{} {} {} {} {} :{}",
      PREFIX, COMMAND, PARAM1, PARAM2, PARAM3, TRAILER
    );
    let command = Command::new(&response).unwrap();
    assert_eq!(command.prefix, PREFIX);
    assert_eq!(command.command, COMMAND);
    assert_eq!(command.params, vec![PARAM1, PARAM2, PARAM3]);
    assert_eq!(command.trailer, TRAILER);
  }

  #[test]
  fn command_parser_only_prefix() {
    let response = format!(":{}", PREFIX);
    assert_eq!(None, Command::new(&response));
  }

  #[test]
  fn command_parser_no_prefix() {
    let response = format!("{} {} {} {} :{}", COMMAND, PARAM1, PARAM2, PARAM3, TRAILER);
    let command = Command::new(&response).unwrap();
    assert_eq!(command.prefix, "");
    assert_eq!(command.command, COMMAND);
    assert_eq!(command.params, vec![PARAM1, PARAM2, PARAM3]);
    assert_eq!(command.trailer, TRAILER);
  }

  #[test]
  fn command_parser_no_prefix_no_parameters() {
    let response = format!("{} :{}", COMMAND, TRAILER);
    let command = Command::new(&response).unwrap();
    assert_eq!(command.prefix, "");
    assert_eq!(command.command, COMMAND);
    assert!(command.params.is_empty());
    assert_eq!(command.trailer, TRAILER);
  }

  #[test]
  fn command_parser_no_prefix_no_trailer() {
    let response = format!("{} {}", COMMAND, PARAM1);
    let command = Command::new(&response).unwrap();
    assert_eq!(command.prefix, "");
    assert_eq!(command.command, COMMAND);
    assert_eq!(command.params, vec![PARAM1]);
    assert_eq!(command.trailer, "");
  }

  #[test]
  fn command_parser_no_trailer() {
    let response = format!(":{} {} {} {} {}", PREFIX, COMMAND, PARAM1, PARAM2, PARAM3);
    let command = Command::new(&response).unwrap();
    assert_eq!(command.prefix, PREFIX);
    assert_eq!(command.command, COMMAND);
    assert_eq!(command.params, vec![PARAM1, PARAM2, PARAM3]);
    assert_eq!(command.trailer, "");
  }

  #[test]
  fn nick_command_encode() {
    let mut codec = IrcCodec::default();
    let message = IrcCommand::Nick(NickCommand {
      nick: String::from("eva"),
    });
    let mut output = BytesMut::new();
    codec
      .encode(message, &mut output)
      .expect("NICK encode error");
    assert_eq!(output, BytesMut::from("NICK eva\x0d\x0a"));
  }

  #[test]
  fn nick_command_decode() {
    let mut codec = IrcCodec::default();
    let mut bytes = BytesMut::from(&b"NICK eva\x0d\x0a"[..]);
    let resp = codec.decode(&mut bytes);
    let expected = Some(vec![IrcMessage::Nick(NickMessage {
      prefix: String::from(""),
      nick: String::from("eva"),
    })]);
    assert_eq!(resp.unwrap(), expected);
  }

  #[test]
  fn nick_command_decode_with_prefix() {
    let mut codec = IrcCodec::default();
    let mut bytes = BytesMut::from(&b":irc.server.com NICK eva\x0d\x0a"[..]);
    let resp = codec.decode(&mut bytes);
    let expected = Some(vec![IrcMessage::Nick(NickMessage {
      prefix: String::from("irc.server.com"),
      nick: String::from("eva"),
    })]);
    assert_eq!(resp.unwrap(), expected);
  }

  #[test]
  fn user_command_encode_without_prefix() {
    let mut codec = IrcCodec::default();
    let message = IrcCommand::User(UserCommand {
      prefix: String::new(),
      username: String::from("eva"),
      mode: UserMode::LocalOperator,
      hostname: String::from("localhost"),
      realname: String::from("Eva R"),
    });
    let mut output = BytesMut::new();
    codec
      .encode(message, &mut output)
      .expect("USER encode error");
    assert_eq!(
      output,
      BytesMut::from("USER eva 0 localhost :Eva R\x0d\x0a")
    );
  }

  #[test]
  fn user_command_encode_with_prefix() {
    let mut codec = IrcCodec::default();
    let message = IrcCommand::User(UserCommand {
      prefix: String::from("irc.server.com"),
      username: String::from("eva"),
      mode: UserMode::Invisible,
      hostname: String::from("localhost"),
      realname: String::from("Eva R"),
    });
    let mut output = BytesMut::new();
    codec
      .encode(message, &mut output)
      .expect("USER encode error");
    assert_eq!(
      output,
      BytesMut::from(":irc.server.com USER eva i localhost :Eva R\x0d\x0a")
    );
  }

  #[test]
  fn notice_command_decode_with_prefix() {
    let mut codec = IrcCodec::default();
    let mut bytes = BytesMut::from(&b":irc.server NOTICE Auth :**Welcome to IRC**\x0d\x0a"[..]);
    let resp = codec.decode(&mut bytes);
    let expected = Some(vec![IrcMessage::Notice(NoticeMessage {
      from: String::from("irc.server"),
      targets: vec![String::from("Auth")],
      message: String::from("**Welcome to IRC**"),
    })]);
    assert_eq!(resp.unwrap(), expected);
  }

  #[test]
  fn notice_command_encode_one_target() {
    let message = IrcCommand::Notice(NoticeCommand {
      targets: vec![String::from("Celina")],
      message: String::from("Jak hejnal brzmi Twoj smiech"),
    });
    let mut output = BytesMut::new();
    let mut codec = IrcCodec::default();
    codec
      .encode(message, &mut output)
      .expect("NOTICE encode error");
    assert_eq!(
      output,
      BytesMut::from("NOTICE Celina :Jak hejnal brzmi Twoj smiech\x0d\x0a")
    );
  }

  #[test]
  fn notice_command_encode_many_targets() {
    let message = IrcCommand::Notice(NoticeCommand {
      targets: vec![String::from("Celina"), String::from("Stanislaw")],
      message: String::from("Jak hejnal brzmi jej smiech"),
    });
    let mut output = BytesMut::new();
    let mut codec = IrcCodec::default();
    codec
      .encode(message, &mut output)
      .expect("NOTICE encode error");
    assert_eq!(
      output,
      BytesMut::from("NOTICE Celina,Stanislaw :Jak hejnal brzmi jej smiech\x0d\x0a")
    );
  }

  #[test]
  fn notice_command_encode_without_prefix_without_params() {
    let message = IrcCommand::Notice(NoticeCommand {
      targets: vec![],
      message: String::from("**Goodbye**"),
    });
    let mut output = BytesMut::new();
    let mut codec = IrcCodec::default();
    codec
      .encode(message, &mut output)
      .expect("NOTICE encode error");
    assert_eq!(output, BytesMut::from("NOTICE :**Goodbye**\x0d\x0a"));
  }

  #[test]
  fn numeric_message_decode() {
    let mut codec = IrcCodec::default();
    let mut bytes = BytesMut::from(":irc.server.com 375 eva :message of the day\x0d\x0a");
    let resp = codec.decode(&mut bytes);

    let expected = Some(vec![IrcMessage::Numeric(NumericMessage {
      prefix: String::from("irc.server.com"),
      code: NumericCode::RplMessageOfTheDayStart,
      params: vec![String::from("eva")],
      trailer: String::from("message of the day"),
    })]);
    assert_eq!(resp.unwrap(), expected);
  }

  #[test]
  fn numeric_message_decode_not_recognized() {
    let mut codec = IrcCodec::default();
    let mut bytes = BytesMut::from(":irc.server.com 999 eva :incorrect message\x0d\x0a");
    let resp = codec.decode(&mut bytes);

    let expected = Some(vec![IrcMessage::Numeric(NumericMessage {
      prefix: String::from("irc.server.com"),
      code: NumericCode::InternalNotRecognize,
      params: vec![String::from("eva")],
      trailer: String::from("incorrect message"),
    })]);
    assert_eq!(resp.unwrap(), expected);
  }

  #[test]
  fn ping_command_encode() {
    let message = IrcCommand::Ping(Ping {
      prefix: String::new(),
      params: vec![String::from("LAG160506605")],
      trailer: String::new(),
    });
    let mut output = BytesMut::new();
    let mut codec = IrcCodec::default();
    codec
      .encode(message, &mut output)
      .expect("DIGIT encode error");
    assert_eq!(output, BytesMut::from("PING LAG160506605\x0d\x0a"));
  }

  #[test]
  fn ping_command_decode() {
    let mut codec = IrcCodec::default();
    let mut bytes = BytesMut::from("PING LAG160506605\x0d\x0a");
    let resp = codec.decode(&mut bytes);

    let expected = Some(vec![IrcMessage::Ping(Ping {
      prefix: String::new(),
      params: vec![String::from("LAG160506605")],
      trailer: String::new(),
    })]);
    assert_eq!(resp.unwrap(), expected);
  }

  #[test]
  fn pong_command_encode() {
    let message = IrcCommand::Pong(Pong {
      prefix: String::from("irc.server.com"),
      params: vec![String::from("irc.server.com")],
      trailer: String::from("LAG160506605"),
    });
    let mut output = BytesMut::new();
    let mut codec = IrcCodec::default();
    codec
      .encode(message, &mut output)
      .expect("DIGIT encode error");
    assert_eq!(
      output,
      BytesMut::from(":irc.server.com PONG irc.server.com :LAG160506605\x0d\x0a")
    );
  }

  #[test]
  fn pong_command_decode() {
    let mut codec = IrcCodec::default();
    let mut bytes = BytesMut::from("PONG LAG160506605\x0d\x0a");
    let resp = codec.decode(&mut bytes);

    let expected = Some(vec![IrcMessage::Pong(Pong {
      prefix: String::new(),
      params: vec![String::from("LAG160506605")],
      trailer: String::new(),
    })]);
    assert_eq!(resp.unwrap(), expected);
  }

  #[test]
  fn quit_command_encode() {
    let message = IrcCommand::Quit(QuitCommand {
      message: String::from("Gone to have lunch"),
    });
    let mut output = BytesMut::new();
    let mut codec = IrcCodec::default();
    codec
      .encode(message, &mut output)
      .expect("DIGIT encode error");
    assert_eq!(output, BytesMut::from("QUIT :Gone to have lunch\x0d\x0a"));
  }

  #[test]
  fn quit_command_decode() {
    let mut codec = IrcCodec::default();
    let mut bytes = BytesMut::from(":irc.server.com QUIT :Gone to have lunch\x0d\x0a");
    let resp = codec.decode(&mut bytes);

    let expected = Some(vec![IrcMessage::Quit(QuitMessage {
      from: String::from("irc.server.com"),
      message: String::from("Gone to have lunch"),
    })]);
    assert_eq!(resp.unwrap(), expected);
  }

  #[test]
  fn pass_command_encode() {
    let message = IrcCommand::Pass(Pass {
      password: String::from("qwerty"),
    });
    let mut output = BytesMut::new();
    let mut codec = IrcCodec::default();
    codec
      .encode(message, &mut output)
      .expect("DIGIT encode error");
    assert_eq!(output, BytesMut::from("PASS qwerty\x0d\x0a"));
  }

  #[test]
  fn oper_command_encode() {
    let message = IrcCommand::Oper(Oper {
      name: String::from("admin"),
      password: String::from("qwerty"),
    });
    let mut output = BytesMut::new();
    let mut codec = IrcCodec::default();
    codec
      .encode(message, &mut output)
      .expect("DIGIT encode error");
    assert_eq!(output, BytesMut::from("OPER admin qwerty\x0d\x0a"));
  }

  #[test]
  fn list_command_encode_without_channels() {
    let message = IrcCommand::List(List { channels: vec![] });
    let mut output = BytesMut::new();
    let mut codec = IrcCodec::default();
    codec
      .encode(message, &mut output)
      .expect("LIST encode error");
    assert_eq!(output, BytesMut::from("LIST\x0d\x0a"));
  }

  #[test]
  fn list_command_encode_with_channels() {
    let message = IrcCommand::List(List {
      channels: vec![String::from("#channel1"), String::from("#channel2")],
    });
    let mut output = BytesMut::new();
    let mut codec = IrcCodec::default();
    codec
      .encode(message, &mut output)
      .expect("LIST encode error");
    assert_eq!(output, BytesMut::from("LIST #channel1,#channel2\x0d\x0a"));
  }

  #[test]
  fn join_command_encode_with_one_complete_channel() {
    let message = IrcCommand::Join(Join {
      channels: vec![Channel::new("#channel1", "key1")],
    });
    let mut output = BytesMut::new();
    let mut codec = IrcCodec::default();
    codec
      .encode(message, &mut output)
      .expect("JOIN encode error");
    assert_eq!(output, BytesMut::from("JOIN #channel1 key1\x0d\x0a"));
  }

  #[test]
  fn join_command_encode_with_two_complete_channels() {
    let message = IrcCommand::Join(Join {
      channels: vec![
        Channel::new("#channel1", "key1"),
        Channel::new("#channel2", "key2"),
      ],
    });
    let mut output = BytesMut::new();
    let mut codec = IrcCodec::default();
    codec
      .encode(message, &mut output)
      .expect("JOIN encode error");
    assert_eq!(
      output,
      BytesMut::from("JOIN #channel1,#channel2 key1,key2\x0d\x0a")
    );
  }

  #[test]
  fn join_command_encode_with_two_complete_channels_and_one_without_key() {
    let message = IrcCommand::Join(Join {
      channels: vec![
        Channel::new("#channel1", "key1"),
        Channel::from("#channel2"),
        Channel::new("#channel3", "key3"),
      ],
    });
    let mut output = BytesMut::new();
    let mut codec = IrcCodec::default();
    codec
      .encode(message, &mut output)
      .expect("JOIN encode error");
    assert_eq!(
      output,
      BytesMut::from("JOIN #channel1,#channel3,#channel2 key1,key3\x0d\x0a")
    );
  }

  #[test]
  fn join_command_encode_with_channels_without_key() {
    let message = IrcCommand::Join(Join {
      channels: vec![
        Channel::from("#channel1"),
        Channel::from("#channel2"),
        Channel::from("#channel3"),
      ],
    });
    let mut output = BytesMut::new();
    let mut codec = IrcCodec::default();
    codec
      .encode(message, &mut output)
      .expect("JOIN encode error");
    assert_eq!(
      output,
      BytesMut::from("JOIN #channel1,#channel2,#channel3\x0d\x0a")
    );
  }
}
